// Settings

// Global Constants
var CLASS_VISIBLE = "visible";
var TEXT_FULL_STAR = "★";
var TEXT_EMPTY_STAR = "☆";
var ID_MENU = "menu";
var ID_ANSWERS = "answers";
var ID_STARS = "stars";
var ID_NEXT_LEVEL = "next-level";
var ID_ANSWER_FIELD = "answer-field";
var CLASS_INACTIVE = "deactivated";

// Global Variables
var selectedImage = 0;
var correctAnswersGiven = 0;
var wrongAnswersGiven = 0;

function decreaseAnswer(min) {
  var answer = getAnswerInField();
  if (answer <= min) {
    answer = min;
  } else {
    answer--;
  }
  setAnswerInField(answer);
  if (answer == min) {
    document.getElementById("minus").classList.add(CLASS_INACTIVE);
  }
  document.getElementById("plus").classList.remove(CLASS_INACTIVE);
  play(answer + "");
}

function increaseAnswer(max) {
  var answer = getAnswerInField();
  if (answer >= max) {
    answer = max;
  } else {
    answer++;
  }
  setAnswerInField(answer);
  if (answer == max) {
    document.getElementById("plus").classList.add(CLASS_INACTIVE);
  }
  document.getElementById("minus").classList.remove(CLASS_INACTIVE);
  play(answer + "");
}

function readAnswer() {
  play(getAnswerInField() + "");
}

function submitAnswer() {
  answer(getAnswerInField() + "");
}

function setAnswerInField(number) {
  var answerField = document.getElementById(ID_ANSWER_FIELD);
  answerField.numericValue = number;
  answerField.innerText = translate(number + "");
}

function getAnswerInField() {
  var answerField = document.getElementById(ID_ANSWER_FIELD);
  // this should be undefined before it is set
  return answerField.numericValue == undefined ? Number(answerField.getAttribute(ATTR_TRANSLATION_KEY)) : answerField.numericValue;
}


function getImages() {
  var root = document.getElementById("pictures");
  return root.getElementsByTagName("img");
}

function selectCurrentImage() {
  var images = getImages();
  for (var i = 0; i < images.length; i++) {
    var image = images[i];
    image.classList.remove(CLASS_VISIBLE);
  }
  if (selectedImage >= 0 && selectedImage < images.length) {
    images[selectedImage].classList.add(CLASS_VISIBLE);
  }
}

function selectNextImage() {
  selectedImage++;
  selectCurrentImage();
}

function thisWasTheLastImage() {
  return getImages().length <= selectedImage;
}

function getCurrentImage() {
  return getImages()[selectedImage];
}

function hideElement(id) {
  var element = document.getElementById(id);
  if (element) {
    element.classList.add("hidden");
  }
}

function showElement(id) {
  var element = document.getElementById(id);
  if (element) {
    element.classList.remove("hidden");
  }
}

function startGame() {
  hideElement(ID_MENU);
  hideElement(ID_STARS);
  showElement(ID_ANSWERS);
}

function stopGame() {
  showElement(ID_MENU);
  showElement(ID_STARS);
  hideElement(ID_ANSWERS);
  calculateStars();
}

function answer(value) {
  var stringValue = value + "";
  var image = getCurrentImage();
  if (image.classList.contains(stringValue)) {
    onCorrectAnswer(stringValue);
  } else {
    onWrongAnswer(stringValue);
  }
}

function onCorrectAnswer(answer) {
  console.log("Correct Answer!");
  selectNextImage();
  correctAnswersGiven++;
  if (thisWasTheLastImage()) {
    stopGame();
  } else {
    //SOUND_ANSWER_RIGHT.play();
    play(answer);
  }
}

function calculateStars() {
  var star1 = document.getElementById("star1");
  var star2 = document.getElementById("star2");
  var star3 = document.getElementById("star3");
  var stars = (wrongAnswersGiven == 0) + (wrongAnswersGiven < correctAnswersGiven) + (wrongAnswersGiven < correctAnswersGiven / 2);
  star1.innerText = stars >= 2 ? TEXT_FULL_STAR : TEXT_EMPTY_STAR;
  star2.innerText = stars >= 1 ? TEXT_FULL_STAR : TEXT_EMPTY_STAR;
  star3.innerText = stars >= 3 ? TEXT_FULL_STAR : TEXT_EMPTY_STAR;
  setStarsForThisLevel(star1.innerText + star2.innerText + star3.innerText); // common.js
  if (stars >= 1) {
    showElement(ID_NEXT_LEVEL);
    confetti.start([1, 50, 1000, 5000][stars]);
  } else {
    hideElement(ID_NEXT_LEVEL);
  }
  SOUND_STARS[stars].play()
}

function onWrongAnswer() {
  console.log("Wrong Answer!");
  wrongAnswersGiven++;
  SOUND_ANSWER_WRONG.play();
}

function loadLevel() {
  selectCurrentImage();
  startGame();
}

window.addEventListener("load", loadLevel);
