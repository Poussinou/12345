/* Sound playing, localized.
 *
 */

var SOUND_FILES = [
  // files for language cy
  "cy/1.ogg",
  "cy/2.ogg",
  "cy/3.ogg",
  "cy/4.ogg",
  "cy/5.ogg",
  "cy/6.ogg",
  // files for language de
  "de/1.ogg",
  "de/2.ogg",
  "de/3.ogg",
  "de/4.ogg",
  "de/5.ogg",
  "de/level-1-intro.ogg",
  "de/level-2-intro.ogg",
  "de/level-3-intro.ogg",
  "de/stars-3.ogg",
  // files for language en
  "en/1.ogg",
  "en/12345-intro-song.ogg",
  "en/2.ogg",
  "en/3.ogg",
  "en/4.ogg",
  "en/5.ogg",
  "en/answer-right.ogg",
  "en/answer-wrong.ogg",
  "en/level-1-intro.ogg",
  "en/level-2-intro.ogg",
  "en/level-3-intro.ogg",
  "en/stars-0.ogg",
  "en/stars-1.ogg",
  "en/stars-2.ogg",
  "en/stars-3.ogg",
];
var SOUND_FILE_FOLDER = "sound";
var ID_AUDIO = "audio-";

function Sound(name) {
  this.name = name;
}

Sound.prototype.play = function() {
  play(this.name);
}

var SOUND_ANSWER_RIGHT = new Sound("answer-right");
var SOUND_ANSWER_WRONG = new Sound("answer-wrong");
var SOUND_STAR_0 = new Sound("stars-0");
var SOUND_STAR_1 = new Sound("stars-1");
var SOUND_STAR_2 = new Sound("stars-2");
var SOUND_STAR_3 = new Sound("stars-3");
var SOUND_STARS = [SOUND_STAR_0, SOUND_STAR_1, SOUND_STAR_2, SOUND_STAR_3];

var currentAudio = {pause: function(){}};

function play(id) {
  var url = translate(ID_AUDIO + id, "");
  if (url == "") {
    console.log("AUDIO: could not find sound " + id);
    return;
  }
  console.log("AUDIO: playing " + id + " as " + url);
  currentAudio.pause();
  currentAudio = new Audio(url);
  currentAudio.play();
}

function translateSoundFiles() {
  SOUND_FILES.forEach(function (file) {
    var match = file.match("^([^/.]+)/([^/.]+)");
    var code = match[1];
    var id = ID_AUDIO + match[2];
    var url = SOUND_FILE_FOLDER + "/" + file;
    setTranslation(code, id, url);
  });
}

window.addEventListener("load", function() {
  translateSoundFiles();
});
