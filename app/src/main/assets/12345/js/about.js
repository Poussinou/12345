
function setVersion(id, value) {
  var element = document.getElementById("version-" + id);
  element.innerText = value;
}

function loadVersionInfoIntoDocument() {
  setVersion("date", VERSION.date);
  setVersion("hash", VERSION.shortHash);
  setVersion("ci", VERSION.ci);
  setVersion("for-user", VERSION.tags.length == 0 ? 
    VERSION.shortHash + (VERSION.branches.length == 0 ?
      "" : " (" + VERSION.branches[0] + ")") :
    VERSION.tags[VERSION.tags.length - 1]);
  setVersion("labels", VERSION.tags.concat(VERSION.branches).join(", "));
}

window.addEventListener("load", function() {
  loadVersionInfoIntoDocument();
});
