
var ID_LANGUAGE = "language-";
var CLASS_FLAG = "flag";
var CLASS_SELECTED_LANGUAGE = "chosen";

function indexTranslationsChanged() {
  var flags = document.getElementsByClassName(CLASS_FLAG);
  for (var i = 0; i < flags.length; i++) {
    var flag = flags[i];
    flag.classList.remove(CLASS_SELECTED_LANGUAGE);
  }
  var lang = getTranslationLanguage().toLowerCase();
  var chosenFlag = document.getElementById(ID_LANGUAGE + lang);
  if (chosenFlag == undefined) {
    var base = lang.split("-")[0];
    chosenFlag = document.getElementById(ID_LANGUAGE + base);
  }
  if (chosenFlag == undefined) {
    console.log("INFO: no flag for " + lang);
    return;
  }
  chosenFlag.classList.add(CLASS_SELECTED_LANGUAGE);
}

function addBrowserLanguageIfNotGiven() {
  
}

window.addEventListener("load", function() {
  notifyAboutChangedTranslations(indexTranslationsChanged);
  addBrowserLanguageIfNotGiven();
});

