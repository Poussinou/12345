#!/bin/bash

cd "`dirname \"$0\"`"
cd "../app/src/main/assets/12345/sound"

sound="../js/sound.js"
tmp="../js/sound.js.tmp"

result="SOUND_FILES = [\n"
for language in *; do
  result="$result  // files for language $language\n"
  for file in "$language"/*; do
    result="$result  \"$file\",\n"
  done
done
result="$result]"
#echo -e "$result"
sed -zE 's|SOUND_FILES[^]]*\]|'"$result"'|' "$sound" > "$tmp"
cat "$tmp" > "$sound"
cat "$sound"
rm "$tmp"
