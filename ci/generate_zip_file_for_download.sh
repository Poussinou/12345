#!/bin/bash

set -e

cd "`dirname \"$0\"`"
cd "../app/src/main/assets/"

zip -r 12345 12345
mv 12345.zip 12345

echo "/* The zip file was added. We have nothing to hide. */" > 12345/css/download-zip.css
